<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\CalculateAccountBalance;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
      $this->app->singleton(CalculateAccountBalance::class, function($app) {
          return new CalculateAccountBalance();
      });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
