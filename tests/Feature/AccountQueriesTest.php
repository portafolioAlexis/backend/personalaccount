<?php

namespace Tests\Feature;

use App\User;
use App\Account;
use Tests\TestCase;
use Laravel\Passport\Passport;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AccountQueriesTest extends TestCase
{
    use RefreshDatabase;

    public function test_it_queries_accounts()
    {
        // prepare
        $user = factory(User::class)->create();
        factory(Account::class,3)->create([
            'user_id' => $user->id
        ]);
        //execute
        Passport::actingAs($user);
        $response = $this->graphQL('
            query {
                accounts(first: 20) {
                    data {
                        id
                        name
                    }
                    paginatorInfo {
                        total
                    }
                }
            }
        ');
        //assert
        $response->assertJson([
            'data' => [
                'accounts' => [
                    'data' => [],
                    'paginatorInfo' => [
                        'total' => 3
                    ]
                ]
            ]
        ]);
    }

    public function test_it_queries_an_account()
    {
        // prepare
        $user = factory(User::class)->create();
        $accounts = factory(Account::class,3)->create([
            'user_id' => $user->id
        ]);
        //execute
        Passport::actingAs($user);
        $account = $accounts->shuffle()->first();//id al azar
        $response = $this->graphQL('
            query {
                account(id: '.$account->id.') {
                    id
                    name
                }
            }
        ');
        //assert
        $response->assertJson([
            'data' => [
                'account' => [
                  'id' => $account->id,
                  'name' => $account->name
                ]
            ]
        ]);
    }

    function test_it_can_query_an_account_not_owned()
    {
      //prepare
        $user = factory(User::class)->create();
        factory(Account::class, 3)->create([
            'user_id' => $user->id
        ]);
        $account = factory(Account::class)->create();
        //execute
        Passport::actingAs($user);
        $response = $this->graphQL('
            query {
                account(id: '.$account->id.') {
                    id
                    name
                }
            }
        ');
        //assert
        $response->assertJson([
            'errors' => [
                [
                  'message' => "You are not authorized to access account"
                ]
            ]
        ]);
    }
}
