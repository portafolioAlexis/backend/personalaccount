<?php

namespace Tests\Feature;

use App\User;
use App\Account;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\TestCase;

class AccountMutationsTest extends TestCase
{
    use RefreshDatabase;

    function test_it_creates_an_account()
    {
      //prepare
      $user = factory(User::class)->create();
      Passport::actingAs($user);
      //execuxion
      $response = $this->graphQL('mutation{
        creatAccount(input: {
          name: "wallet",
          balance: 200
        }){
          name
          balance
          user {
            id
            name
          }
        }
      }');
      //assertions
      $response->assertJson([
        'data' =>[
          'creatAccount' => [
            'name' => 'wallet',
            'balance' => 200.00,
            'user' => [
              'id' => $user->id,
              'name' => $user->name
            ]
          ]
        ]
      ]);
      $this->assertDatabaseHas('accounts',[
        'name' => 'wallet',
        'balance' => '200',
        'user_id' => $user->id
      ]);
    }

    function test_it_validates_input()
    {
      //prepare
      $user = factory(User::class)->create();
      Passport::actingAs($user);
      //execuxion
      $response = $this->graphQL('
      mutation{
        creatAccount(input: {
          name: "wallet",
          balance: "sdasdasd"
        }){
          name
          balance
          user {
            id
            name
          }
        }
      }');
      //assertions
      $response->assertJson([
        'errors' =>[
          [
            "message" => "Field \"creatAccount\" argument \"input\" requires type Float!, found \"sdasdasd\"."
          ]
        ]
      ]);
      $this->assertDatabaseMissing('accounts',[
        'name' => 'wallet',
        'user_id' => $user->id
      ]);
    }

    function test_it_validates_balance_no_less_than_0()
    {
      //prepare
      $user = factory(User::class)->create();
      Passport::actingAs($user);
      //execuxion
      $response = $this->graphQL('
      mutation{
        creatAccount(input: {
          name: "wallet",
          balance: -50
        }){
          name
          balance
          user {
            id
            name
          }
        }
      }');
      //assertions
      $response->assertJson([
        'errors' =>[
          [
            'message' => "Validation failed for the field [creatAccount].",
            'extensions' => [
                'validation' => [
                  'input.balance' => ['The input.balance must be greater than or equal 0.']
                ]
              ]
            ]
          ]
      ]);
      $this->assertDatabaseMissing('accounts',[
        'name' => 'wallet',
        'user_id' => $user->id
      ]);
    }

    function test_it_can_update_an_account()
    {
      $user = factory(User::class)->create();
      $account = factory(Account::class)->create([
        'name' => 'wallet',
        'user_id' => $user->id,
        'balance' => 100
      ]);
      Passport::actingAs($user);
      //execuxion
      $response = $this->graphQL('
      mutation{
        updateAccount(id: '.$account->id.', input: {
          name: "Savings",
        }){
          id
          name
          balance
        }
      }');

      //assertions
      $response->assertJson([
        'data' =>[
            'updateAccount' => [
                'id' => $account->id,
                'name' => 'Savings',
                'balance' => $account->balance
              ]
          ]
      ]);

      $this->assertDatabaseHas('accounts',[
        'user_id' => $user->id,
        'name' => 'Savings',
        'id' => $account->id
      ]);
    }

    function test_it_cant_update_an_account_when_not_owner()
    {
      $user = factory(User::class)->create();
      $user2 = factory(User::class)->create();
      $account = factory(Account::class)->create([
        'name' => 'wallet',
        'user_id' => $user->id,
        'balance' => 100
      ]);
      Passport::actingAs($user2);
      //execuxion
      $response = $this->graphQL('
      mutation{
        updateAccount(id: '.$account->id.', input: {
          name: "Savings",
        }){
          id
          name
          balance
        }
      }');

      //assertions
      $response->assertJson([
        'errors' =>[
            [
              'message' => "You are not authorized to access updateAccount"
            ]
          ]
      ]);

      $this->assertDatabaseMissing('accounts',[
        'user_id' => $user->id,
        'name' => 'Savings',
        'id' => $account->id
      ]);
    }
}
