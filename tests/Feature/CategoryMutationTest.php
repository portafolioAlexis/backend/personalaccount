<?php

namespace Tests\Feature;

use App\User;
use App\Category;
use Laravel\Passport\Passport;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CategoryMutationTest extends TestCase
{
    use RefreshDatabase;
    function test_it_can_create_a_category()
    {
        //Prepare
        $user = factory(User::class)->create();
        Passport::actingAs($user);
        //Execute
        $response = $this->graphQL('
            mutation{
              createCategory(input: {
                name: "category 1",
              }){
                name
                user {
                  id
                  name
                }
              }
        }');
        //assert
        $response->assertJson([
          'data' =>[
            'createCategory' => [
              'name' => 'category 1',
              'user' => [
                'id' => $user->id,
                'name' => $user->name
              ]
            ]
          ]
        ]);
    }

    function test_it_can_update_a_category()
    {
        //Prepare
        $user = factory(User::class)->create();
        $category = factory(Category::class)->create([
          'user_id' => $user->id,
        ]);
        Passport::actingAs($user);
        //Execute
        $response = $this->graphQL('
            mutation{
              updateCategory(id: "'.$category->id.'",input: {
                name: "category 1",
              }){
                name
                user {
                  id
                  name
                }
              }
        }');
        //assert
        $response->assertJson([
          'data' =>[
            'updateCategory' => [
              'name' => 'category 1',
              'user' => [
                'id' => $user->id,
                'name' => $user->name
              ]
            ]
          ]
        ]);
    }

    function test_it_cant_update_a_category_when_not_owner()
    {
      $user = factory(User::class)->create();
      $user2 = factory(User::class)->create();
      $category = factory(Category::class)->create([
        'user_id' => $user->id,
      ]);
      Passport::actingAs($user2);
      //execuxion
      $response = $this->graphQL('
      mutation{
        updateCategory(id: '.$category->id.', input: {
          name: "Savings",
        }){
          id
          name
        }
      }');

      //assertions
      $response->assertJson([
        'errors' =>[
            [
              'message' => "You are not authorized to access updateCategory"
            ]
          ]
      ]);

      $this->assertDatabaseMissing('categories',[//tableName
        'user_id' => $user->id,
        'name' => 'Savings',
        'id' => $category->id
      ]);
    }

    function test_it_can_delete_a_category()
    {
      $user = factory(User::class)->create();
      $category = factory(Category::class)->create([
        'user_id' => $user->id,
      ]);
      Passport::actingAs($user);
      //execuxion
      $response = $this->graphQL('
        mutation {
          deleteCategory(id: '.$category->id.' ) {
              id
              name
          }
        }
      ');

      //assertions
      $response->assertJson([
        'data' =>[
            'deleteCategory' => [
              'id' => $category->id
            ]
          ]
      ]);
      $this->assertNotNull($category->fresh()->deleted_at);
    }

    function test_it_cant_delete_a_category_when_not_owner()
    {
      $user = factory(User::class)->create();
      $user2 = factory(User::class)->create();
      $category = factory(Category::class)->create([
        'user_id' => $user->id,
      ]);
      Passport::actingAs($user2);
      //execuxion
      $response = $this->graphQL('
        mutation {
          deleteCategory(id: '.$category->id.' ) {
              id
              name
          }
        }
      ');

      //assertions
      $response->assertJson([
        'errors' =>[
            [
              'message' => "You are not authorized to access deleteCategory"
            ]
          ]
      ]);

      $this->assertDatabaseMissing('categories',[//tableName
        'user_id' => $user->id,
        'name' => 'Savings',
        'id' => $category->id
      ]);
      $this->assertNull($category->fresh()->deleted_at);
    }
}
