<?php

namespace Tests\Feature;

use App\User;
use App\Account;
use App\Category;
use App\Transaction;
use Laravel\Passport\Passport;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class TransactionMutationsTest extends TestCase
{
    use RefreshDatabase;

    function test_it_creates_transaction_and_update_balance()
    {
      //prepare
      $user = factory(User::class)->create();
      $account = factory(Account::class)->create([
        'user_id' => $user->id,
        'balance' => 0
      ]);
      $category = factory(Category::class)->create([
        'user_id' => $user->id
      ]);
      Passport::actingAs($user);
      //execuxion
      $response = $this->graphQL('mutation{
        createTransaction(input: {
            account_id: ' .$account->id. ',
            category_id: ' .$category->id. ',
            type: INCOME,
            desciption: "INCOME",
            amount: 100
        }){
          desciption
          type
          amount
          account{
            id
            name
            balance
          }
        }
      }');
      //assertions
      $response->assertJson([
        'data' =>[
          'createTransaction' => [
            'desciption' => 'INCOME',
            'type' => 'INCOME',
            'amount' => 100.00,
            'account' => [
              'id' => $account->id,
              'name' => $account->name,
              'balance' => 100.00
            ]
          ]
        ]
      ]);
    }

    function test_it_creates_transaction_and_update_balance_with_expense()
    {
      //prepare
      $user = factory(User::class)->create();
      $account = factory(Account::class)->create([
        'user_id' => $user->id,
        'balance' => 100
      ]);
      $category = factory(Category::class)->create([
        'user_id' => $user->id
      ]);
      Passport::actingAs($user);
      //execuxion
      $response = $this->graphQL('mutation{
        createTransaction(input: {
          account_id: '.$account->id.',
          category_id: '.$category->id.',
          type: EXPENSE,
          desciption: "Expense",
          amount: 50
        }){
          desciption
          type
          amount
          account{
            id
            name
            balance
          }
        }
      }');
      //assertions
      $response->assertJson([
        'data' =>[
          'createTransaction' => [
            'desciption' => 'Expense',
            'type' => 'EXPENSE',
            'amount' => 50.00,
            'account' => [
              'id' => $account->id,
              'name' => $account->name,
              'balance' => 50.00
            ]
          ]
        ]
      ]);

    }

    function test_it_can_update_a_transaction()
    {
      //prepare
      $user = factory(User::class)->create();
      $account = factory(Account::class)->create([
        'user_id' => $user->id,
        'balance' => 100
      ]);
      $transaction = factory(Transaction::class)->state('income')->create([
        'account_id' => $account->id,
        'amount' => 50
      ]);
      $this->assertEquals(150,$account->fresh()->balance);
      Passport::actingAs($user);
      //execuxion
      $response = $this->graphQL('
      mutation{
        updateTransaction(id: '.$transaction->id.', input: {
          amount: 20
        }){
          desciption
          type
          amount
          account{
            id
            name
            balance
          }
        }
      }');

      //assertions
      $response->assertJson([
        'data' =>[
          'updateTransaction' => [
            'amount' => 20.00,
            'account' => [
              'balance' => 120.00
            ]
          ]
        ]
      ]);
    }

    function test_it_can_update_a_transaction_case_2()
    {
      //prepare
      $user = factory(User::class)->create();
      $account = factory(Account::class)->create([
        'user_id' => $user->id,
        'balance' => 100
      ]);
      $transaction = factory(Transaction::class)->state('expense')->create([
        'account_id' => $account->id,
        'amount' => 50
      ]);
      $this->assertEquals(50,$account->fresh()->balance);
      Passport::actingAs($user);
      //execuxion
      $response = $this->graphQL('
      mutation{
        updateTransaction(id: '.$transaction->id.', input: {
          amount: 20
        }){
          desciption
          type
          amount
          account{
            id
            name
            balance
          }
        }
      }');

      //assertions
      $response->assertJson([
        'data' =>[
          'updateTransaction' => [
            'amount' => 20.00,
            'account' => [
              'balance' => 80.00
            ]
          ]
        ]
      ]);
    }

    function test_it_cant_update_a_transaction_when_not_owner()
    {
      //prepare
      $user = factory(User::class)->create();
      $user2 = factory(User::class)->create();
      $account = factory(Account::class)->create([
        'user_id' => $user->id,
        'balance' => 100
      ]);
      $transaction = factory(Transaction::class)->state('expense')->create([
        'account_id' => $account->id,
        'amount' => 50
      ]);
      $this->assertEquals(50,$account->fresh()->balance);
      Passport::actingAs($user2);
      //execuxion
      $response = $this->graphQL('
      mutation{
        updateTransaction(id: '.$transaction->id.', input: {
          amount: 20
        }){
          desciption
          type
          amount
          account{
            id
            name
            balance
          }
        }
      }');

      //assertions
      $response->assertJson([
        'errors' =>[
            [
              'message' => "You are not authorized to access updateTransaction"
            ]
          ]
      ]);
    }

    function test_it_can_delete_a_transaction()
    {
      //prepare
      $user = factory(User::class)->create();
      $account = factory(Account::class)->create([
        'user_id' => $user->id,
        'balance' => 100
      ]);
      $transaction = factory(Transaction::class)->state('expense')->create([
        'account_id' => $account->id,
        'amount' => 50
      ]);
      $this->assertEquals(50,$account->fresh()->balance);
      Passport::actingAs($user);
      //execuxion
      $response = $this->graphQL('
      mutation{
        deleteTransaction(id: '.$transaction->id.'){
          desciption
          type
          amount
          account{
            id
            name
            balance
          }
        }
      }');

      //assertions
      $response->assertJson([
        'data' =>[
          'deleteTransaction' => [
            'account' => [
              'balance' => 100.00
            ]
          ]
        ]
      ]);
    }
    function test_it_can_delete_a_transaction_case_2()
    {
      //prepare
      $user = factory(User::class)->create();
      $account = factory(Account::class)->create([
        'user_id' => $user->id,
        'balance' => 100
      ]);
      $transaction = factory(Transaction::class)->state('income')->create([
        'account_id' => $account->id,
        'amount' => 50
      ]);
      $this->assertEquals(150,$account->fresh()->balance);
      Passport::actingAs($user);
      //execuxion
      $response = $this->graphQL('
      mutation{
        deleteTransaction(id: '.$transaction->id.'){
          desciption
          type
          amount
          account{
            id
            name
            balance
          }
        }
      }');
      //assertions
      $response->assertJson([
        'data' =>[
          'deleteTransaction' => [
            'account' => [
              'balance' => 100.00
            ]
          ]
        ]
      ]);
    }
}
