<?php

namespace Tests\Feature;

use App\User;
use App\Category;
use Tests\TestCase;
use Laravel\Passport\Passport;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryQueriesTest extends TestCase
{
    use RefreshDatabase;
    public function test_it_queries_categories()
    {
        // prepare
        $user = factory(User::class)->create();
        factory(Category::class,3)->create([
            'user_id' => $user->id
        ]);
        //execute
        Passport::actingAs($user);
        $response = $this->graphQL('
            query {
                categories(first: 20) {
                    data {
                        id
                        name
                    }
                    paginatorInfo {
                        total
                    }
                }
            }
        ');
        //assert
        $response->assertJson([
            'data' => [
                'categories' => [
                    'data' => [],
                    'paginatorInfo' => [
                        'total' => 3
                    ]
                ]
            ]
        ]);
    }

    public function test_it_queries_an_category()
    {
        // prepare
        $user = factory(User::class)->create();
        $category = factory(Category::class,3)->create([
            'user_id' => $user->id
        ]);
        //execute
        Passport::actingAs($user);
        $category = $category->shuffle()->first();//id al azar
        $response = $this->graphQL('
            query {
                category(id: '.$category->id.') {
                    id
                    name
                }
            }
        ');
        //assert
        $response->assertJson([
            'data' => [
                'category' => [
                  'id' => $category->id,
                  'name' => $category->name
                ]
            ]
        ]);
    }

    function test_it_can_query_an_categories_not_owned()
    {
      //prepare
        $user = factory(User::class)->create();
        $category = factory(Category::class)->create();
        //execute
        Passport::actingAs($user);
        $response = $this->graphQL('
            query {
                category(id: '.$category->id.') {
                    id
                    name
                }
            }
        ');
        //assert
        $response->assertJson([
            'errors' => [
                [
                  'message' => "You are not authorized to access category"
                ]
            ]
        ]);
    }
}
